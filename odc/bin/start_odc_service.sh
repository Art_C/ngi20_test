#!/bin/sh

echo "start to startup odc procedures"

export LD_LIBRARY_PATH=/usr/odc/lib:/usr/rpc/lib:/usr/local/lib:${LD_LIBRARY_PATH}
export CINEMO_PLUGIN_INIT=/usr/local/lib/cinemo

/usr/rpc/bin/rpcserver&

sleep 1

cd /usr/odc/bin/

echo "Start ODC PowerModeApp"

./PowerModeApp&

echo "Start ODC TunerApp"

./TunerApp&

echo "Start ODC MediaApp"

./MediaApp&

