#!/bin/sh

ps aux|grep rpcserver|grep -v grep
if [ $? -eq 0 ]; then
   killall -q -9 rpcserver
fi

ps aux|grep PowerModeApp|grep -v grep
if [ $? -eq 0 ]; then
   killall -q -9 PowerModeApp
fi

ps aux|grep TunerApp|grep -v grep
if [ $? -eq 0 ]; then
   killall -q -9 TunerApp
fi

ps aux|grep MediaApp|grep -v grep
if [ $? -eq 0 ]; then
   killall -q -9 MediaApp
fi

exit
